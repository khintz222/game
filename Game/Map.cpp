#include "Map.h"

void Map::draw()
{
	for (Objects* element : map)
	{
		element->update(map);
		element->draw();
	}
}
