#include "MainCharacter.h"
#include "Moving.h"

void MainCharacter::update(std::list<Objects*> map)
{
	this->collisionClear();
	this->collisions(map);
	if (updateTime.timing())
	{
		clear();
		if (GetAsyncKeyState(VK_UP) && position.Y > 1)
		{
			if(upCollision)
			{
				if(upCollision->isStatic == false)
				{
					if (upCollision->moveUp(map))
					position.Y--;
				}
			}else
			{
				position.Y--;
			}
			
		}
		else if (GetAsyncKeyState(VK_DOWN) && position.Y < 29)
		{

			if (downCollision)
			{
				if (downCollision->isStatic == false)
				{
					if (downCollision->moveDown(map))
					position.Y++;
				}
			}
			else
			{
				position.Y++;
			}
			
			
		}
		else if (GetAsyncKeyState(VK_LEFT) && position.X > 0)
		{
			if (leftCollision)
			{
				if (leftCollision->isStatic == false)
				{
					if (leftCollision->moveLeft(map))
						position.X--;
				}
			}
			else
			{
				position.X--;
			}
			
			
		}
		else if (GetAsyncKeyState(VK_RIGHT) && position.X < 118 )
		{
			if (rightCollision)
			{
				if (rightCollision->isStatic == false)
				{
					if (rightCollision->moveRight(map))
						position.X++;
				}
			}
			else
			{
				position.X++;
			}
			
		}
	}
}

void MainCharacter::draw()
{
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);
	std::cout << look;
}
