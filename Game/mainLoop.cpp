﻿#include <iostream>
#include <ctime>
#include "Map.h"
#include "test.h"
#include "MainCharacter.h"
#include "Moving.h"

using namespace std;

void HideCursor()
{
	HANDLE hConsoleOut = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO hCCI;
	GetConsoleCursorInfo(hConsoleOut, &hCCI);
	hCCI.bVisible = FALSE;
	SetConsoleCursorInfo(hConsoleOut, &hCCI);
}

void ShowCursor()
{
	HANDLE hConsoleOut = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO hCCI;
	GetConsoleCursorInfo(hConsoleOut, &hCCI);
	if (hCCI.bVisible != TRUE)
	{
		hCCI.bVisible = TRUE;
		SetConsoleCursorInfo(hConsoleOut, &hCCI);
	}
}

int main()
{
	CONSOLE_CURSOR_INFO hCCI;
	GetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &hCCI);

SetConsoleTitle( "GAME" );
	list<Objects*> mO;
	Objects* player = new MainCharacter({0, 1});
	mO.push_back(player);

	for (short i = 0; i < 30; i++)
	{
		mO.push_back(new test('|', {(short)rand() % 50 + 40, (short)rand() % 15 + 5}, rand() % 5));
	}
	mO.push_back(new Moving({ 2,3 }));
	mO.push_back(new Moving({ 4,3 }));
	mO.push_back(new Moving({ 6,3 }));
	mO.push_back(new Moving({ 8,3 }));

	mO.push_back(new Moving({ 3,4 }));
	mO.push_back(new Moving({ 5,4 }));
	mO.push_back(new Moving({ 7,4 }));


	mO.push_back(new Moving({ 2,5 }));
	mO.push_back(new Moving({ 4,5 }));
	mO.push_back(new Moving({ 6,5 }));
	mO.push_back(new Moving({ 8,5 }));


	


	Map map1(mO);
	int frames = 0,FPS=0;
	clock_t start=clock();
	while (!GetAsyncKeyState(VK_ESCAPE))
	{
		frames++;
		if (start + 1000 <= clock())
		{
			start = clock();
			FPS = frames;
			frames = 0;
		}
		
			if (hCCI.bVisible == TRUE)HideCursor();
			SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), { 0, 0 });
		
		
		cout << "FPS= " << FPS <<" { ";
		player->getPosition();
		cout << " } ESC to close ";
		map1.draw();
	}
}
