#pragma once


/**
 * \brief obiekt kt�ry dziedziczy z tej klasy da si� narysowa� 
 */
class Drawable
{
public:
	virtual void draw() = 0;

	Drawable()
	{
	}

	~Drawable() = default;
};
