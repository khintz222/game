#pragma once
#include "Character.h"


class MainCharacter :
	public Character
{
public:
	MainCharacter(COORD pos) : Character(pos, '*')
	{
	}

	void update(std::list<Objects*> map) override;
	void draw() override;

	~MainCharacter() = default;
};
