#pragma once
#include <ctime>

/**
 * \brief biblioteka czasu gry 
 */
class Time
{
private:
	clock_t frameTime;
	clock_t nextFrame;
public:

	Time(int);

	bool timing();
};
