#include "Time.h"


Time::Time(int FPS): frameTime(FPS != 0 ? 1000 / FPS : 0), nextFrame(clock() + frameTime)
{
}

bool Time::timing()
{
	if (frameTime == 0)return false;
	if (clock() >= nextFrame)
	{
		nextFrame += frameTime;
		return true;
	}
	return false;
}
