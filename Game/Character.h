﻿#pragma once

#include "Objects.h"
#include <iostream>

class Character :
	public Objects
{
public:
	Character(COORD pos, char z): Objects(pos, z, 15)
	{
	}

	void update(std::list<Objects*> map) override = 0;
	void draw() override = 0;

	void clear()
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);
		std::cout << ' ';
	}

	~Character() = default;
};
