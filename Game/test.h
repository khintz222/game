#pragma once
#include "Objects.h"
#include <iostream>

class test :
	public Objects
{
public:
	test(char z, COORD pos, int FPS): Objects(pos, z, FPS)
	{
	}

	void draw() override
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), position);
		std::cout << look;
	}

	void update(std::list<Objects*> map) override
	{
		if (updateTime.timing())
		{
			if (look == '|')
			{
				look = '/';
			}
			else if (look == '/')
			{
				look = '-';
			}
			else if (look == '-')
			{
				look = '\\';
			}
			else
			{
				look = '|';
			}
		}
	}

	void collisions(std::list<Objects*> map) override
	{
	}

	~test() = default;
};
