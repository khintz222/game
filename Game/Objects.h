#pragma once
#include <windows.h>
#include "Time.h"
#include "Drawable.h"
#include <list>
#include <iostream>


class Objects : Drawable
{
protected:
	COORD position;
	char look;
	Time updateTime;

	Objects* upCollision = nullptr;
	Objects* downCollision = nullptr;
	Objects* rightCollision = nullptr;
	Objects* leftCollision = nullptr;
	Objects* onCollision = nullptr;


	
	


public:
	Objects();


	virtual bool moveRight(std::list<Objects*> map)
	{
		this->collisions(map);
		
		if (position.X < 118 && !rightCollision)
		{
			position.X++;
			this->collisionClear();
			return true;
		}
		this->collisionClear();
		return false;
		
	}
	virtual bool moveLeft(std::list<Objects*> map)
	{
		this->collisions(map);
		
		if (position.X > 0 && !leftCollision)
		{
			position.X--;
			this->collisionClear();
			return true;
		}
		this->collisionClear();
		return false;
	}
	virtual bool moveUp(std::list<Objects*> map)
	{
		this->collisions(map);
		
		if (position.Y > 1 && !upCollision)
		{
			position.Y--;
			this->collisionClear();
			return true;
		}
		this->collisionClear();
		return false;
	}
	virtual bool moveDown(std::list<Objects*> map)
	{
		this->collisions(map);
		
		if (position.Y < 29 && !downCollision)
		{
			position.Y++;
			this->collisionClear();
			return true;
		}
		this->collisionClear();
		return false;
	}
	Objects(COORD pos, char z, int uT): position(pos), look(z), updateTime(uT)
	{
	}

	virtual void update(std::list<Objects*> map) = 0;
	virtual void collisionClear();

	void getPosition();
	bool isStatic = true;
	void draw() override = 0;
	~Objects() = default;

	virtual void collisions(std::list<Objects*> map);
};
