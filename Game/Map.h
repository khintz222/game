#pragma once
#include <list>
#include "Objects.h"
#include "Drawable.h"


class Map : Drawable
{
	static int x, y;
	std::list<Objects*> map;

public:
	Map(std::list<Objects*> objects)
	{
		for (Objects* element : objects)
		{
			map.push_back(element);
		}
	}

	void draw() override;


	~Map() = default;
};
