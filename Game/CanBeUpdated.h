#pragma once
class CanBeUpdated
{
private:
	char look;
public:
	virtual void update() = 0;
	CanBeUpdated();
	CanBeUpdated(char look):look(look){}
	~CanBeUpdated();
};

