#include "Objects.h"
#include <iostream>

void Objects::collisionClear()
{
	if (upCollision)
		upCollision = nullptr;
	if (downCollision)
		downCollision = nullptr;
	if (rightCollision)
		rightCollision = nullptr;
	if (leftCollision)
		leftCollision = nullptr;
}

void Objects::getPosition()
{
	std::cout << position.X << ", " << position.Y;
}

void Objects::collisions(std::list<Objects*> map)
{
	for (Objects* element : map)
	{
		if (this->position.X == element->position.X)
		{
			if (this->position.Y == element->position.Y + 1)
			{
				if (this != element)
					upCollision = element;
			}
			if (this->position.Y == element->position.Y - 1)
			{
				if (this != element)
					downCollision = element;
			}
		}
		else if (this->position.Y == element->position.Y)
		{
			if (this->position.X == element->position.X + 1)
			{
				if (this != element)
					leftCollision = element;
			}
			if (this->position.X == element->position.X - 1)
			{
				if (this != element)
					rightCollision = element;
			}
		}
	}
}
